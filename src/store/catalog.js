// ------------------------------------
// Constants
// ------------------------------------
export const CATALOG_FETCH_REQUEST = 'CATALOG_FETCH_REQUEST'
export const CATALOG_FETCH_SUCCESS = 'CATALOG_FETCH_SUCCESS'
export const CATALOG_FETCH_ERROR = 'CATALOG_FETCH_ERROR'

export const CATALOG_ITEM_ADD_TO_CART = 'CATALOG_ITEM_ADD_TO_CART'
export const CATALOG_CART_CLEAR = 'CATALOG_CART_CLEAR'

export const CART_CHECKOUT_SUCCESS = 'CART_CHECKOUT_SUCCESS'

// ------------------------------------
// Actions
// ------------------------------------
export function fetchCatalogRequest () {
  return {
    type: CATALOG_FETCH_REQUEST
  }
}
export function fetchCatalogSuccess (data) {
  return {
    type: CATALOG_FETCH_SUCCESS,
    payload: data
  }
}
export function fetchCatalogError (error) {
  return {
    type: CATALOG_FETCH_ERROR,
    payload: error
  }
}
export const fetchCatalog = () => {
  return (dispatch) => {
    return new Promise((resolve) => {
      dispatch(fetchCatalogRequest())
      fetch('http://beta.json-generator.com/api/json/get/4kiDK7gxZ')
        .then(response => response.json())
        .then(json => dispatch(fetchCatalogSuccess(json)))
        .then(() => resolve())
        .catch(error => dispatch(fetchCatalogError(error)))
    })
  }
}

export function addItemToCart (itemId) {
  return {
    type: CATALOG_ITEM_ADD_TO_CART,
    payload: itemId
  }
}

export function clearCart () {
  return {
    type: CATALOG_CART_CLEAR
  }
}

export function checkoutSucess () {
  return {
    type: CART_CHECKOUT_SUCCESS
  }
}

export const checkout = () => {
  return (dispatch) => {
    // do some async requests for checkout
    dispatch(clearCart())
    dispatch(checkoutSucess())
  }
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [CATALOG_FETCH_REQUEST]: (state) => ({
    ...state,
    status: 'loading'
  }),
  [CATALOG_FETCH_SUCCESS]: (state, action) => {
    const byId = action.payload.reduce(
      (acc, val) => ({ ...acc, [val._id]: val }),
      {}
    )
    return {
      ...state,
      status: 'success',
      byId,
      all: Object.keys(byId)
    }
  },
  [CATALOG_FETCH_ERROR]: (state) => ({
    ...initialState,
    status: 'error'
  }),
  [CATALOG_ITEM_ADD_TO_CART]: (state, action) => {
    const itemId = action.payload

    const item = { ...state.byId[itemId] }
    let itemsRemaining = item.stock ? item.stock.remaining : 0
    if (!itemsRemaining) return state
    itemsRemaining--
    item.stock.remaining = itemsRemaining
    let byId = { ...state.byId }
    byId[itemId] = item

    const cart = { ...state.cart }
    cart[itemId] = cart[itemId] ? cart[itemId] + 1 : 1

    return {
      ...state,
      checkoutStatus: '',
      byId,
      cart
    }
  },
  [CATALOG_CART_CLEAR]: (state) => ({
    ...state,
    cart: {}
  }),
  [CART_CHECKOUT_SUCCESS]: (state) => ({
    ...state,
    checkoutStatus: 'success'
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  status: '',
  checkoutStatus: '',
  byId: {},
  all: [],
  cart: {}
}
export default function reducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
