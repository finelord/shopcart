import React from 'react'
import { IndexLink, Link } from 'react-router'
import PropTypes from 'prop-types'
import './PageLayout.scss'

export const PageLayout = ({ children }) => (
  <div>
    <div className='page-layout__content container'>
      <nav className='page-layout__nav nav nav-pills'>
        <IndexLink
          to='/'
          className='nav-link'
          activeClassName='page-layout__nav-item--active active'
        >
          home
        </IndexLink>
        <Link
          to='/grid'
          className='nav-link'
          activeClassName='page-layout__nav-item--active active'
        >
          grid
        </Link>
        <Link
          to='/cart'
          className='nav-link'
          activeClassName='page-layout__nav-item--active active'
        >
          cart
        </Link>
      </nav>
      <div className='page-layout__viewport'>
        {children}
      </div>
    </div>

  </div>
)
PageLayout.propTypes = {
  children: PropTypes.node
}

export default PageLayout
