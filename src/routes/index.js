import CoreLayout from '../layouts/PageLayout/PageLayout'
import Home from './Home'
import GridRoute from './Grid'
import CartRoute from './Cart'

export const createRoutes = (store) => ({
  path        : '/',
  component   : CoreLayout,
  indexRoute  : Home,
  childRoutes : [
    GridRoute(store),
    CartRoute(store)
  ]
})

export default createRoutes
