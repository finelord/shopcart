// ------------------------------------
// Constants
// ------------------------------------
export const GRID_SHOW_ITEM_DETAILS = 'GRID_SHOW_ITEM_DETAILS'
export const GRID_HIDE_ITEM_DETAILS = 'GRID_HIDE_ITEM_DETAILS'

// ------------------------------------
// Actions
// ------------------------------------
export function showGridItemDetails (data) {
  return {
    type: GRID_SHOW_ITEM_DETAILS,
    payload: data
  }
}
export function hideGridItemDetails () {
  return {
    type: GRID_HIDE_ITEM_DETAILS
  }
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [GRID_SHOW_ITEM_DETAILS]: (state, action) => ({
    ...state,
    selectedItemId: action.payload
  }),
  [GRID_HIDE_ITEM_DETAILS]: (state) => ({
    ...state,
    selectedItemId: null
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  selectedItemId: null
}
export default function reducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
