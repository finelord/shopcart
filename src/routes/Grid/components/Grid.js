import React from 'react'
import PropTypes from 'prop-types'
import GridItemContainer from '../containers/GridItemContainer'
import GridItemDetailsContainer from '../containers/GridItemDetailsContainer'

class Grid extends React.Component {
  componentWillMount () {
    const { gridItemIds, fetchCatalog } = this.props
    if (gridItemIds.length === 0) {
      fetchCatalog()
    }
  }
  render () {
    const { gridItemIds, selectedItemId } = this.props
    return (
      <div>
        <div className='card-columns'>
          {
            gridItemIds.map(id =>
              <GridItemContainer gridItemId={id} key={id} />
            )
          }
          {
            selectedItemId !== null &&
            <GridItemDetailsContainer />
          }
        </div>
      </div>
    )
  }
}

Grid.propTypes = {
  gridItemIds: PropTypes.array.isRequired,
  selectedItemId: PropTypes.string,
  fetchCatalog: PropTypes.func.isRequired
}

export default Grid
