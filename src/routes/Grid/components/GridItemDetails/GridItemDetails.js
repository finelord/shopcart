import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'

import './GridItemDetails.scss'

export const GridItem = ({
  gridItem,
  hideGridItemDetails,
  addItemToCart
}) => (
  <div className='grid-item-details__container'>
    <div className='modal-dialog'>
      <div className='modal-content'>
        <div className='modal-header'>
          <h5 className='modal-title'>{gridItem.title}</h5>
          <button
            type='button'
            className='close'
            aria-label='close'
            onClick={() => hideGridItemDetails(gridItem._id)}
          >
            <span aria-hidden='true'>&times;</span>
          </button>
        </div>
        <div className='modal-body'>
          <img
            src={gridItem.image}
            alt={gridItem.title}
            className='grid-item-details__image img-thumbnail'
          />
          <p>{gridItem.description}</p>
          <h4>price: {gridItem.price}</h4>
          <p>remaining: {gridItem.stock.remaining}</p>
        </div>
        <div className='modal-footer'>
          <button
            type='button'
            className='btn btn-secondary'
            onClick={() => hideGridItemDetails()}
          >
            close
          </button>
          <Link
            to='/cart'
            className='btn btn-secondary'
            onClick={() => hideGridItemDetails()}
          >
            go to cart
          </Link>
          <button
            type='button'
            className='btn btn-primary'
            disabled={gridItem.stock.remaining === 0}
            onClick={() => addItemToCart(gridItem._id)}
          >
            add to cart
          </button>
        </div>
      </div>
    </div>
  </div>
)
GridItem.propTypes = {
  gridItem: PropTypes.object.isRequired,
  hideGridItemDetails: PropTypes.func.isRequired,
  addItemToCart: PropTypes.func.isRequired
}

export default GridItem
