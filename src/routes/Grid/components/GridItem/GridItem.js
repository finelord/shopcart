import React from 'react'
import PropTypes from 'prop-types'
import './GridItem.scss'

export const GridItem = ({
  gridItem,
  showGridItemDetails,
  addItemToCart
}) => (
  <div className='card text-center'>
    <div
      className='grid-item__content card-block'
      onClick={() => showGridItemDetails(gridItem._id)}
    >
      <h4 className='card-title'>{gridItem.title}</h4>
      <img
        className='img-thumbnail'
        src={gridItem.image}
        alt={gridItem.title}
      />
    </div>
    <h5>{gridItem.price}</h5>
    {
      gridItem.stock.remaining === 0 &&
      <p>out of stock</p>
    }
    <p>
      <button
        type='button'
        className='btn btn-secondary'
        disabled={gridItem.stock.remaining === 0}
        onClick={() => addItemToCart(gridItem._id)}>add to cart</button>
    </p>
  </div>
)
GridItem.propTypes = {
  gridItem: PropTypes.object.isRequired,
  showGridItemDetails: PropTypes.func.isRequired,
  addItemToCart: PropTypes.func.isRequired
}

export default GridItem
