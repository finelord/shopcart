import { injectReducer } from '../../store/reducers'

export default (store) => ({
  path : 'grid',
  getComponent (nextState, cb) {
    require.ensure([], (require) => {
      const Grid = require('./containers/GridContainer').default
      const reducer = require('./modules/grid').default

      injectReducer(store, { key: 'grid', reducer })

      cb(null, Grid)
    }, 'grid')
  }
})
