import { connect } from 'react-redux'
import { hideGridItemDetails } from '../modules/grid'
import { addItemToCart } from 'store/catalog'

import GridItemDetails from '../components/GridItemDetails'

const mapDispatchToProps = {
  hideGridItemDetails,
  addItemToCart
}

const mapStateToProps = (state, ownProps) => ({
  gridItem: state.catalog.byId[state.grid.selectedItemId]
})

export default connect(mapStateToProps, mapDispatchToProps)(GridItemDetails)
