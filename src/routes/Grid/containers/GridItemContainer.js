import { connect } from 'react-redux'
import { showGridItemDetails } from '../modules/grid'
import { addItemToCart } from 'store/catalog'

import GridItem from '../components/GridItem'

const mapDispatchToProps = {
  showGridItemDetails,
  addItemToCart
}

const mapStateToProps = (state, ownProps) => ({
  gridItem: state.catalog.byId[ownProps.gridItemId]
})

export default connect(mapStateToProps, mapDispatchToProps)(GridItem)
