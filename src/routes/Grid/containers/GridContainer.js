import { connect } from 'react-redux'
import { fetchCatalog } from 'store/catalog'

import Grid from '../components/Grid'

const mapDispatchToProps = {
  fetchCatalog
}

const mapStateToProps = (state) => ({
  gridItemIds: state.catalog.all,
  selectedItemId: state.grid.selectedItemId
})

export default connect(mapStateToProps, mapDispatchToProps)(Grid)
