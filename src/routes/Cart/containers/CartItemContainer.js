import { connect } from 'react-redux'

import CartItem from '../components/CartItem'

const mapDispatchToProps = {

}

const mapStateToProps = (state, ownProps) => ({
  cartItem: state.catalog.byId[ownProps.cartItemId],
  cartItemCount: state.catalog.cart[ownProps.cartItemId]
})

export default connect(mapStateToProps, mapDispatchToProps)(CartItem)
