import { connect } from 'react-redux'
import { checkout } from 'store/catalog'

import Cart from '../components/Cart'

const mapDispatchToProps = {
  checkout
}

const mapStateToProps = (state) => ({
  cartItemIds: Object.keys(state.catalog.cart),
  checkoutStatus: state.catalog.checkoutStatus
})

export default connect(mapStateToProps, mapDispatchToProps)(Cart)
