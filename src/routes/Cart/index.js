export default (store) => ({
  path : 'cart',
  getComponent (nextState, cb) {
    require.ensure([], (require) => {
      const Cart = require('./containers/CartContainer').default

      cb(null, Cart)
    }, 'cart')
  }
})
