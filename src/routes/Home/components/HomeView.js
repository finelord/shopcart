import React from 'react'
import { Link } from 'react-router'
import './HomeView.scss'

export const HomeView = () => (
  <div>
    <h1>welcome</h1>
    <Link to='/grid'>
      ---&gt; go shopping &lt;---
    </Link>
  </div>
)

export default HomeView
