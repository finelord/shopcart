# shopcart

## start

```bash
$ npm i
$ npm start
```

## details

based on [React Redux Starter Kit](https://github.com/davezuko/react-redux-starter-kit)

technologies: react + redux + react router + bootstrap built by webpack

this stack was chosen because of:

- it allows to create prototypes rapidly
- redux allows to build scalable web apps with complex logic and explicit way of data flows manipulation => good starting point for a dynamic app with growth potential. also, it splits view and business logic
- react component architecture allows to create reusable parts of code; there's a very simple api, huge community and big amount of devs who know this technology
- bootstrap is a perfect way to create prototypes quickly

## todo

- reorganise store, extract cart store from catalog
- extract common reusable components such as notification
- features cush a cart items counter in nav, summary in the shop cart